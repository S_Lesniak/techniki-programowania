#include <iostream>
#include <iomanip>


using namespace std;

int main()

{
    void wyswietl(int **tab, int liczb);
    int **liczby;
    int numer;
    cout<<"Jaki rozmiar tabliczki? ";
    cin>>numer;
    liczby = new int*[numer];
    for(int i=0;i<numer;i++)
    {
        liczby[i]=new int[numer];
    }
    for(int i=0;i<numer;i++)
    {
        for(int j=0;j<numer;j++)
        {
            liczby[i][j]=(i+1)*(j+1);
        }
    }
    cout<<endl;
    wyswietl(liczby,numer);

    return 0;
}

void wyswietl(int **tab,int liczb)
{
    for(int i=0;i<liczb;i++)
    {
        for(int j=0;j<liczb;j++)
        {
            cout<<setw(4);
            cout<<tab[i][j];

        }
        cout<<endl;
    }
}

