#include "VectorInt.h"

VectorInt::VectorInt()
{
    _dlugosc = 16;
    _size = 0;
    _capacity = _dlugosc;
    _wek = new int[_dlugosc];
    for (int i = 0; i<_dlugosc; i++)
    {
        _wek[i] = 0;
    }
}

VectorInt::VectorInt(int n)
{
    _dlugosc = n;
    _capacity = _dlugosc;
    _wek = new int [_dlugosc];
    for (int i = 0; i < _dlugosc; i++)
    {
        _wek[i] = 0;
    }
}

VectorInt::VectorInt(const VectorInt& Copy)
{
    this->_dlugosc = Copy._dlugosc;
    this->_size = Copy._size;
    this->_wek = new int [_dlugosc];
    for (int i = 0; i < _dlugosc; i++)
    {
        this->_wek[i] = Copy._wek[i];
    }
}

VectorInt& VectorInt::operator = (const VectorInt& Copy)
{
    if (this != &Copy)
    {
        delete this->_wek;
        this->_dlugosc = Copy._dlugosc;
        this->_size = Copy._size;
        this->_wek = new int [_dlugosc];
        for (int i = 0; i < _dlugosc; i++)
        {
            _wek[i] = Copy._wek[i];
        }
    }
    return *this;
}

void VectorInt::relok(int n)
{
    int tab[n];
    for (int i = 0; i < n; i++)
    {
        if(i < _dlugosc)
        {
            tab[i] = _wek[i];
        }
        else
        {
            tab[i] = 0;
        }
    }
    delete [] _wek;
    _dlugosc = n;
    _wek = new int [_dlugosc];
    for (int i = 0; i < _dlugosc; i++)
    {
        _wek[i] = tab[i];
    }
}

int VectorInt::Capacity()
{
    return _capacity;
}

int VectorInt::get_dlugosc()
{
    return _dlugosc;
}

int VectorInt::Size()
{
    return _size;
}

int VectorInt::At(int n)
{
    return _wek[n-1];
}

void VectorInt::Insert(int n, int w)
{
    if (n <= 0)
    {
        cout<<"Indeks od 1!"<<endl;
    }
    else if (n > _dlugosc)
    {
        relok(n);

        _wek[n-1] = w;
    }
    else
    {
        _wek[n-1] = w;
    }
    check_size();
}

void VectorInt::Clear()
{
    for(int i = 0; i < _dlugosc; i++)
    {
        _wek[i] = 0;
    }
    check_size();
}

void VectorInt::PushBack(int n)
{
    if(_capacity == 0)
    {
        relok(_dlugosc+1);
        _wek[_dlugosc-1] = n;
    }
    else
    {
        _wek[_size] = n;
    }
    check_size();
}

void VectorInt::PopBack()
{
    if(_size > 0)
    {
        _wek[_size-1] = 0;
        check_size();
    }
    else
    {
        cout<<"Wektor pusty! Nie ma co usuwac."<<endl;
    }
}

void VectorInt::ShrinkToFit()
{
    check_size();
    relok(_size);
    _capacity = 0;
}

void VectorInt::check_size()
{
    bool pom = true;
    for(int i = _dlugosc-1; i >= 0; i--)
    {
        if(_wek[i]!=0)
        {
            _size = i+1;
            pom = false;
            break;
        }
    }
    if(pom) _size = 0;
    _capacity = _dlugosc-_size;
}

namespace patch

{
    template < typename T > string to_string( const T& n )
    {
        ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

ostream& operator<<(ostream& out,VectorInt& wek)
{
    string ds="[ ";
    for(int i =0;i<wek.Size()-1;i++)
    {
        ds=ds+ patch::to_string(wek.At(i))+", ";
    }
    ds=ds+ patch::to_string(wek.At(wek.Size()-1))+" ]";

    return out<<ds;
}

VectorInt::~VectorInt()
{
    _size = 0;
    _capacity = 0;
    _dlugosc = 0;
    delete [] _wek;
}
