#include <iostream>
#include "VectorInt.h"
#include <windows.h>

int main()
{
    VectorInt w1;
    bool dziala = true;
        int n;
        int opcja;
        cout<<"Witaj drogi uzytkowniku!"<<endl<<flush;
        cout<<"Stworz swoj wektor."<<endl<<"Nacisnij jakas liczbe, aby kontynuowac. Nacisnij 0 aby wyjsc z programu:"<<endl<<flush;
        cin>>n;
        if(n == 0)
        {
            return 1;
        }
        else
        {
            system("cls");
            do
        {

            cout<<"Pamiec na "<<w1.get_dlugosc()<<" miejsc."<<endl<<flush;
            cout<<"Liczba wymiarow: "<<w1.Size()<<endl<<flush;
            cout<<"Liczba wolnych miejsc: "<<w1.Capacity()<<endl<<flush;
            cout<<"Wybierz opcje, 0 - wyjscie z programu:"<<endl<<flush;
            cout<<"1. Wyswietl dany element wektora."<<endl<<flush;
            cout<<"2. Wstawic wartosc na miejsce."<<endl<<flush;
            cout<<"3. Wstawic wartosc na wierzch."<<endl<<flush;
            cout<<"4. Usunac liczbe z wierzchu."<<endl<<flush;
            cout<<"5. Usunac caly wektor."<<endl<<flush;
            cout<<"6. Usunac niewykorzystywana pamiec."<<endl<<flush;
            cin>>opcja;
            if (opcja == 0)
            {
                system("cls");
                cout<<"Dziekuje za uzytkowanie! :D"<<endl<<flush;
                dziala = false;
                system("PAUSE");
                system("cls");
                return 0;
            }
            else
            {
                switch(opcja)
                {
                    case 1:
                    {
                        int pole;
                        cout<<"Ktory element chcesz zobaczyc? (zakres: 1-"<<w1.get_dlugosc()<<")"<<endl<<flush;
                        cout<<"Wpisz numer pola:"<<endl<<flush;
                        cin>>pole;
                        cout<<endl;
                        if((pole <=0)||(pole > w1.get_dlugosc()))
                        {
                            cout<<"Prosze wybrac liczbe z zakresu!"<<endl<<flush;
                        }
                        else
                        {
                            cout<<"V_"<<pole<<"="<<w1.At(pole)<<endl<<flush;
                            system("pause");
                            system("cls");
                        }
                        break;
                    }
                    case 2:
                        {
                            int m, w;
                            cout<<"Na jakim miejscu chcesz wstawic wartosc:"<<endl<<flush;
                            cin>>m;
                            cout<<endl;
                            cout<<"Jaka wartosc:"<<endl<<flush;
                            cin>>w;
                            w1.Insert(m, w);
                            system("cls");
                            break;
                        }
                    case 3:
                        {
                            int w;
                            cout<<"Podaj wartosc:"<<endl<<flush;
                            cin>>w;
                            cout<<endl;
                            w1.PushBack(w);
                            system("cls");
                            break;
                        }
                    case 4:
                        {
                            w1.PopBack();
                            system("cls");
                            break;
                        }
                    case 5:
                        {
                            w1.Clear();
                            system("cls");
                            break;
                        }
                    case 6:
                        {
                            w1.ShrinkToFit();
                            system("cls");
                            break;
                        }
                }
            }
        }
            while(dziala);
    return 0;
        }
}

