#ifndef VECTORINT_H
#define VECTORINT_H

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

class VectorInt
{
    public:
        VectorInt();
        ~VectorInt();
        VectorInt(int);
        VectorInt(const VectorInt&);
        VectorInt& operator = (const VectorInt&);
        int Capacity();
        int get_dlugosc();
        int Size();
        int At(int);
        void Clear();
        void ShrinkToFit();
        void PushBack(int);
        void PopBack();
        void Insert(int, int);
        void relok(int);
        void check_size();


    private:
        int _size;
        int _capacity;
        int _dlugosc;
        int *_wek;


};

ostream& operator<<(ostream&,VectorInt&);


#endif // VECTORINT_H
